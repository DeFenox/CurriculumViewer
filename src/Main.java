import com.defenox.curview.controller.Controller;
import com.defenox.curview.model.Model;
import com.defenox.curview.view.View;

/**
 * Created by defenox on 27.10.16.
 */
public class Main {

    public static void main(String[] args) {

        Model theModel = new Model();
        View theView = new View();

        Controller theController = new Controller(theModel, theView);

    }
}
