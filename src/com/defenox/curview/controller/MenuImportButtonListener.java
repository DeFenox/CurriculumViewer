package com.defenox.curview.controller;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;


/**
 * Created by defenox on 27.10.16.
 */
public class MenuImportButtonListener implements ActionListener {

    private Controller controller;

    /**
     * Set controller
     * @param controller Controller object
     */
    public MenuImportButtonListener(Controller controller) {
        this.controller = controller;
    }

    /**
     * Start import, parse XML file
     * @param e ActionEvent object
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        System.out.println(JFileChooser.APPROVE_OPTION);
        JFileChooser fileopen = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("XML FILES", "xml", "xml");
        fileopen.setFileFilter(filter);
        int ret = fileopen.showDialog(null, "Открыть файл");
        if (ret == JFileChooser.APPROVE_OPTION) {
            File file = fileopen.getSelectedFile();

            String extension = getExtensionFile(file.getName());

            if(extension.equalsIgnoreCase("xml")){
                controller.loadFile(file);
                controller.parseDataXML();
                controller.setTreeIntoView();
            }else {
                try {
                    throw new FileNotFoundException("This is file have wrong extension. The extension of file must be \"xml\"");
                } catch (FileNotFoundException ex) {
                    controller.createDisplayErrorDialog(ex.getMessage());
                }
            }
        }
    }

    private String getExtensionFile(String fileName){
        int i = fileName.lastIndexOf('.');
        return fileName.substring(i+1);
    }
}
