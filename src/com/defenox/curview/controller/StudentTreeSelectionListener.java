package com.defenox.curview.controller;

import com.defenox.curview.model.entity.Entity;
import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

/**
 * Created by DeFenox on 13.11.2016.
 */
public class StudentTreeSelectionListener implements TreeSelectionListener {

    Controller controller;

    /**
     * Set controller
     * @param controller Controller object
     */
    public StudentTreeSelectionListener(Controller controller) {
        this.controller = controller;
    }

    /**
     *
     * {@inheritDoc}
     */
    @Override
    public void valueChanged(TreeSelectionEvent e) throws ParseException {
        TreePath path = e.getPath();
        int pathCount = path.getPathCount();
        if(pathCount > 1) {
            DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) path.getPathComponent(pathCount - 1);
            Entity entity = (Entity) treeNode.getUserObject();
            try {
                controller.setTable(entity.displayText());
            } catch (java.text.ParseException ex) {
                controller.createDisplayErrorDialog(ex.getMessage());
            }
        }else {
            controller.setTable(new JTable());
        }
    }
}
