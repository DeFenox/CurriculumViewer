package com.defenox.curview.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by defenox on 27.10.16.
 */
public class MenuExitButtonListener implements ActionListener{

    private Controller controller;

    /**
     * Set controller
     * @param controller Controller object
     */
    public MenuExitButtonListener(Controller controller) {
        this.controller = controller;
    }

    /**
     *
     * @param e ActionEvent object
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        System.exit(0);
    }
}
