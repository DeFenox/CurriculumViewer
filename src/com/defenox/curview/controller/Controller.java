package com.defenox.curview.controller;

import com.defenox.curview.model.Model;
import com.defenox.curview.view.View;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

/**
 * Created by defenox on 27.10.16.
 */
public class Controller {

    private Model theModel;
    private View theView;

    /**
     * Returns a Controller with the specified Model and View
     * @param theModel a Model object
     * @param theView a View object
     */
    public Controller(Model theModel, View theView) {
        this.theModel = theModel;
        this.theView = theView;
        addListeners();
    }

    /**
     * Add listeners for import and exit items menu
     */
    private void addListeners(){
        theView.addActionListenerForImport(new MenuImportButtonListener(this));
        theView.addActionListenerForExit(new MenuExitButtonListener(this));
    }

    /**
     * Set XML file which need parse
     * @param file XML file with data
     */
    public void loadFile(File file) {
        theModel.setFile(file);
    }

    /**
     * Parse data from XML file
     */
    public void parseDataXML() {
        try {
            theModel.parseXML();
        } catch (ParserConfigurationException e) {
            createDisplayErrorDialog(e.getMessage());
        } catch (SAXException e) {
            createDisplayErrorDialog(e.getMessage());
        } catch (IOException e) {
            createDisplayErrorDialog(e.getMessage());
        }
    }

    /**
     * Add listener to JTree and set JTree into Model
     */
    public void setTreeIntoView(){
        JTree tree = theModel.getTree();
        tree.addTreeSelectionListener(new StudentTreeSelectionListener(this));
        theView.addTree(tree);
    }

    /**
     * Call error dialog from view
     * @param errorMessage String object message of error
     */
    public void createDisplayErrorDialog(String errorMessage){
        theView.displayErrorDialog(errorMessage);
    }

    /**
     * Set table to view
     * @param table JTable object
     */
    public void setTable(JTable table) {
        theView.addTable(table);
    }
}
