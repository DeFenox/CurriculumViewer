package com.defenox.curview.view;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.awt.*;
import java.awt.event.ActionListener;


/**
 * Created by defenox on 27.10.16.
 */
public class View extends JFrame {
    private static final int FRAME_WIDTH = 640;
    private static final int FRAME_HEIGHT = 480;

    private static final String FRAME_TITLE = "Main Frame";

    private JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
    private JMenuBar jmb = new JMenuBar();
    private JMenuItem jmiImport;
    private JMenuItem jmiExit;

    private Dimension minimumSize = new Dimension(100, 50);
    private Dimension maximumSize = new Dimension(150, 50);

    public View(){
        initializeView(FRAME_TITLE);
    }

    /**
     * Initialize main view with the special param of frame title
     * @param titleFrame String object
     */
    private void initializeView(String titleFrame) {

        this.setLayout(new BorderLayout());
        this.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        this.setMinimumSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
        this.getRootPane().setDoubleBuffered(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle(titleFrame);

        //set position window on center of screen
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);

        this.setJMenuBar(jmb);

        createMenu();
        createFrame();

        this.add(splitPane);
        this.setVisible(true);
    }

    /**
     * Display error dialog with special message
     * @param errorMessage String error massage
     * @return string of enter data
     */
    public void displayErrorDialog(String errorMessage){
        JOptionPane.showMessageDialog(this, errorMessage, "Error", JOptionPane.ERROR_MESSAGE);
    }

    private void createFrame() {
        splitPane.setLeftComponent(new JTree(new DefaultMutableTreeNode("Students")));
        splitPane.setRightComponent(new JTable());
        splitPane.setOneTouchExpandable(true);
        splitPane.setDividerLocation(150);

    }

    private void createMenu() {
        JMenu jmFile = new JMenu("File");

        jmiImport = new JMenuItem("Import from XML...");

        jmiExit = new JMenuItem("Exit");
        jmFile.add(jmiImport);
        jmFile.addSeparator();
        jmFile.add(jmiExit);

        jmb.add(jmFile);
    }

    /**
     * Set listener for import item of menu
     * @param listener ActionListener object
     */
    public void addActionListenerForImport(ActionListener listener){
        jmiImport.addActionListener(listener);
    }

    /**
     * Set listener for exit item of menu
     * @param listener ActionListener object
     */
    public void addActionListenerForExit(ActionListener listener){
        jmiExit.addActionListener(listener);
    }

    /**
     * Set tree in view
     * @param tree JTree object
     */
    public void addTree(JTree tree){
        tree.setMinimumSize(minimumSize);
        tree.setMaximumSize(maximumSize);
        JScrollPane treeView = new JScrollPane(tree);
        splitPane.setLeftComponent(treeView);
    }

    /**
     * Set table in view
     * @param table
     */
    public void addTable(JTable table){
        table.setShowGrid(false);
        table.setMinimumSize(minimumSize);
        splitPane.setRightComponent(table);
    }
}
