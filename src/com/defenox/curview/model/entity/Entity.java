package com.defenox.curview.model.entity;

import javax.swing.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Main interface for all entities.
 *
 * Created by defenox on 01.11.16.
 */
public interface Entity {

    /**
     * Date format for all entities.
     * Used in displayText() method.
     */
    DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

    /**
     * Is used to display entity
     * @return string that represent entity.
     */
    JTable displayText() throws ParseException;
}