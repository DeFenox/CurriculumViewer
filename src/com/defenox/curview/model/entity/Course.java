package com.defenox.curview.model.entity;

import javax.swing.*;
import java.text.ParseException;
import java.util.*;

/**
 * Created by defenox on 03.11.16.
 */
public class Course implements Entity{

    private String id;
    private String name;
    private String author;
    private String dateOfCreation;
    private Map<String, Task> listTask = new HashMap<>();

    /**
     * Return id of object
     * @return String object
     */
    public String getId() {
        return id;
    }

    /**
     * Set id of course
     * @param id String object
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Get name of course
     * @return String object
     */
    public String getName() {
        return name;
    }

    /**
     * Set name of course
     * @param name String object
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get full name of author
     * @return String object
     */
    public String getAuthor() {
        return author;
    }

    /**
     * Set full name of author
     * @param author String object
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * Get date creation of course
     * @return String object
     */
    public String getDateOfCreation() {
        return dateOfCreation;
    }

    /**
     * Set date creation of course
     * @param dateOfCreation
     * @throws ParseException
     */
    public void setDateOfCreation(String dateOfCreation) throws ParseException {
        Date date = dateFormat.parse(dateOfCreation);
        this.dateOfCreation = dateFormat.format(date);
    }

    /**
     *  Get list of tasks
     * @return Map&lt;String, Task&gt; object
     */
    public Map<String, Task> getListTask() {
        return listTask;
    }

    /**
     * Set list of tasks
     * @param listTask Map&lt;String, Task&gt; object
     */
    public void setListTask(Map<String, Task> listTask) {
        this.listTask = listTask;
    }

    /**
     * List of ids course
     * @param coursesId String[] object
     */
    public void setIdCourses(String[] coursesId) {
        for (String idTask : coursesId) {
            listTask.put(idTask, null);
        }
    }

    /**
     * Return name of course
     * @return String object
     */
    @Override
    public String toString() {
        return name;
    }

    /**
     * Return info about course in table
     * @return JTable object
     */
    @Override
    public JTable displayText() {

        Vector<Vector> rowData = new Vector<>();
        rowData.addElement(createRow("Course name: " + name));
        rowData.addElement(createRow("Author: " + author));
        rowData.addElement(createRow("Date of creation: " + dateOfCreation));
        rowData.addElement(createRow(""));

        List<String> taskNames = getTasksNames();

        for (String taskName : taskNames) {
            rowData.addElement(createRow(taskName));
        }

        Vector<String> columnNames = new Vector<>();
        columnNames.addElement("Course information");

        JTable table = new JTable(rowData, columnNames);

        return table;
    }

    /**
     * Create row from one string element
     * @param sValue String object
     * @return Vector&lt;String&gt;
     */
    private Vector<String> createRow(String sValue){
        Vector<String> row = new Vector<>();
        row.addElement(sValue);
        return row;
    }

    /**
     * Get list of task's names
     * @return List&lt;String&gt;
     */
    private List<String> getTasksNames(){
        List<String> tasksNames = new LinkedList<>();
        if(!listTask.isEmpty()){
            tasksNames.add("Tasks:");
        }
        for (Map.Entry<String, Task> taskEntry : listTask.entrySet()){
            tasksNames.add(taskEntry.getValue().getName());
        }
        return tasksNames;
    }
}
