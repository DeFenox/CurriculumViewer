package com.defenox.curview.model.entity;

import javax.swing.*;
import java.text.ParseException;
import java.util.*;

/**
 * Created by defenox on 03.11.16.
 */
public class Program implements Entity{

    private String id;
    private String name;
    private String author;
    private String dateOfCreation;
    private Map<String, Course> listCourse = new HashMap<>();

    /**
     * Get id of program
     * @return String object
     */
    public String getId() {
        return id;
    }

    /**
     * Set id of program
     * @param id String object
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Get name of program
     * @return String object
     */
    public String getName() {
        return name;
    }

    /**
     * Get name of program
     * @param name String object
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get author name
     * @return String object
     */
    public String getAuthor() {
        return author;
    }

    /**
     * Set author name
     * @param author String object
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * Get date creation of program
     * @return String object
     */
    public String getDateOfCreation() {
        return dateOfCreation;
    }

    /**
     * Set date creation of program
     * @param dateOfCreation String object
     * @throws ParseException
     */
    public void setDateOfCreation(String dateOfCreation) throws ParseException {
        Date date = dateFormat.parse(dateOfCreation);
        this.dateOfCreation = dateFormat.format(date);
    }

    /**
     * Get id of program
     * @return Map&lt;String, Course&gt; object
     */
    public Map<String, Course> getListCourse() {
        return listCourse;
    }

    /**
     * Get id of program
     * @param listCourse Map&lt;String, Course&gt; object
     */
    public void setListCourse(Map<String, Course> listCourse) {
        this.listCourse = listCourse;
    }

    /**
     * Set list of ids program
     * @param idCourses String[] object
     */
    public void setIdCourses(String[] idCourses) {
        for (String idCourse : idCourses) {
            listCourse.put(idCourse, null);
        }
    }

    /**
     * Return name of program
     * @return String object
     */
    @Override
    public String toString() {
        return name;
    }

    /**
     * Return info about program in table
     * @return JTable object
     */
    @Override
    public JTable displayText() {

        Vector<Vector> rowData = new Vector<>();
        rowData.addElement(createRow("Program name: " + name));
        rowData.addElement(createRow("Author: " + author));
        rowData.addElement(createRow("Date of creation: " + dateOfCreation));
        rowData.addElement(createRow(""));

        List<String> courseNames = getCoursesNames();

        for (String courseName : courseNames) {
            rowData.addElement(createRow(courseName));
        }

        Vector<String> columnNames = new Vector<>();
        columnNames.addElement("Program information");

        JTable table = new JTable(rowData, columnNames);
        return table;
    }

    /**
     * Create row from one string element
     * @param sValue String object
     * @return Vector&lt;String&gt;
     */
    private Vector<String> createRow(String sValue){
        Vector<String> row = new Vector<>();
        row.addElement(sValue);
        return row;
    }

    /**
     * Get list of courses's names
     * @return List&lt;String&gt;
     */
    private List<String> getCoursesNames(){
        List<String> coursesNames = new LinkedList<>();
        if(!listCourse.isEmpty()){
            coursesNames.add("Courses:");
        }
        for (Map.Entry<String, Course> courseEntry : listCourse.entrySet()){
            coursesNames.add(courseEntry.getValue().getName());
        }
        return coursesNames;
    }
}