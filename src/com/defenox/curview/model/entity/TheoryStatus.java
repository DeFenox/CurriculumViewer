package com.defenox.curview.model.entity;

/**
 * Created by defenox on 03.11.16.
 */
public enum TheoryStatus implements Status {

    NOT_STARTED("Studying not started"),
    IN_PROCESS("Studying in process"),
    COMPLETE("Studying is complete");

    private final String status;

    TheoryStatus(String s) {
        this.status = s;
    }

    /**
     * Return full name of status
     * @return String object
     */
    @Override
    public String getFullNameStatus() {
        return this.status;
    }
}
