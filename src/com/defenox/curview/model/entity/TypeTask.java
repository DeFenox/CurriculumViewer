package com.defenox.curview.model.entity;

/**
 * Created by defenox on 03.11.16.
 */
public enum TypeTask {

    THEORY, PRACTICAL
}