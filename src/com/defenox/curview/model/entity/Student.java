package com.defenox.curview.model.entity;

import javax.swing.*;
import java.text.ParseException;
import java.util.*;

/**
 * Created by defenox on 03.11.16.
 */
public class Student implements Entity{

    private String id;
    private String fullName;
    private String city;
    private String email;
    private String date;
    private String endDate;
    private List<Program> programList = new ArrayList<>();
    private boolean isSignContract;

    public Student() {
    }

    /**
     * Get id of student
     * @return String object
     */
    public String getId() {
        return id;
    }

    /**
     * Set id of student
     * @param id String object
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Get full name of student
     * @return String object
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * Set full name of student
     * @param fullName String object
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * Get city of student
     * @return String object
     */
    public String getCity() {
        return city;
    }

    /**
     * Set city of student
     * @param city String object
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * Get email of student
     * @return String object
     */
    public String getEmail() {
        return email;
    }

    /**
     * Set email of student
     * @param email String object
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Get dste of student
     * @return String object
     */
    public String getDate() {
        return date;
    }

    /**
     * Set date of student
     * @param sDate String object
     * @throws ParseException
     */
    public void setDate(String sDate) throws ParseException {
        Date date = dateFormat.parse(sDate);
        this.date =  dateFormat.format(date);
    }

    /**
     * Get end date of student
     * @return String object
     */
    public String getEndDate() {
        return endDate;
    }

    /**
     * Set end date of student
     * @param endDate String object
     * @throws ParseException
     */
    public void setEndDate(String endDate) throws ParseException {
        Date date = dateFormat.parse(endDate);
        this.endDate = dateFormat.format(date);
    }

    /**
     * Get sign contract of student
     * @return Boolean object
     */
    public boolean isSignContract() {
        return isSignContract;
    }

    /**
     * Set sign contracr of student
     * @param signContract Boolean object
     */
    public void setSignContract(boolean signContract) {
        isSignContract = signContract;
    }

    /**
     * Get list of program
     * @return List&lt;Program&gt object
     */
    public List<Program> getProgramList() {
        return programList;
    }

    /**
     * Set list of program
     * @param programList List&lt;Program&gt object
     */
    public void setProgramList(List<Program> programList) {
        this.programList = programList;
    }

    /**
     * Get full name of student
     * @return String object
     */
    @Override
    public String toString() {
        return fullName;
    }

    /**
     * Return info about students in table
     * @return JTable object
     * @throws ParseException
     */
    @Override
    public JTable displayText() throws ParseException {

        String signed = isSignContract ? "Signed" : ":Unsigned";

        Vector<Vector> rowData = new Vector<>();
        rowData.addElement(createRow("Full name: " + fullName));
        rowData.addElement(createRow("City: " + city));
        rowData.addElement(createRow("E-mail: " + email));
        rowData.addElement(createRow("Start Date: " + date));
        rowData.addElement(createRow("Contract: " + signed));

        rowData.addElement(createRow(""));

        calculateDateToEnd();

        rowData.addElement(createRow("End date: " + endDate));

        Vector<String> columnNames = new Vector<>();
        columnNames.addElement("Student information");

        JTable table = new JTable(rowData, columnNames);
        return table;
    }

    /**
     * Create row from one string element
     * @param sValue String object
     * @return Vector&lt;String&gt;
     */
    private Vector<String> createRow(String sValue){
        Vector<String> row = new Vector<>();
        row.addElement(sValue);
        return row;
    }

    /**
     * Calculate date of end program
     * @throws ParseException
     */
    private void calculateDateToEnd() throws ParseException {

        int totalHours = 0;
        int durationOfSchoolDay = 8;

        for (Program programElement : programList) {

            Map<String, Course> courses = programElement.getListCourse();

            for (Map.Entry<String, Course> courseElement : courses.entrySet()) {

                Map<String, Task> tasks = courseElement.getValue().getListTask();

                for (Map.Entry<String, Task> taskElement : tasks.entrySet()) {
                    totalHours += Integer.parseInt(taskElement.getValue().getDuration());
                }
            }
        }

        int days = (int) Math.ceil(totalHours / durationOfSchoolDay);

        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(dateFormat.parse(date));
        calendar.add(Calendar.DAY_OF_YEAR, days);

        setEndDate(dateFormat.format(calendar.getTime()));
    }
}
