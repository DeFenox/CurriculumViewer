package com.defenox.curview.model.entity;

/**
 * Created by defenox on 03.11.16.
 */
public interface Status {

    /**
     * Return full name of status
     * @return String object
     */
    String getFullNameStatus();
}
