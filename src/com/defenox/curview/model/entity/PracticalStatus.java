package com.defenox.curview.model.entity;

/**
 * Created by defenox on 03.11.16.
 */
public enum PracticalStatus implements Status {

    NOT_STARTED("Execution not started"),
    IN_PROCESS("Execution in process"),
    REVIEWED("Being reviewed"),
    ACCEPTED("The task is accepted");

    private final String status;

    PracticalStatus(String s) {
        this.status = s;
    }

    /**
     * Get full name of status
     * @return String object
     */
    @Override
    public String getFullNameStatus() {
        return this.status;
    }
}
