package com.defenox.curview.model.entity;

import javax.swing.*;

/**
 * Created by defenox on 03.11.16.
 */
public class Task implements Entity{

    private String id;
    private String name;
    private String duration;
    private String mark;
    private TypeTask type;
    private Status status;

    /**
     * Get id of task
     * @return String object
     */
    public String getId() {
        return id;
    }

    /**
     * Set id of task
     * @param id String object
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Get name of task
     * @return String object
     */
    public String getName() {
        return name;
    }

    /**
     * Set name of task
     * @param name String object
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get duration of task
     * @return String object
     */
    public String getDuration() {
        return duration;
    }

    /**
     * Set duration of task
     * @param duration String object
     */
    public void setDuration(String duration) {
        this.duration = duration;
    }

    /**
     * Get mark of task
     * @return String object
     */
    public String getMark() {
        return mark;
    }

    /**
     * Set mark of task
     * @param mark String object
     */
    public void setMark(String mark) {
        this.mark = mark;
    }

    /**
     * Get type of task
     * @return TypeTask object
     */
    public TypeTask getType() {
        return type;
    }

    /**
     * Get type of task
     * @param type TypeTask object
     */
    public void setType(TypeTask type) {
        this.type = type;
    }

    /**
     * Get status of task
     * @return Status object
     */
    public Status getStatus() {
        return status;
    }

    /**
     * Get status of task
     * @param status Status object
     */
    public void setStatus(Status status) {
        this.status = status;
    }

    /**
     * Return name of task
     * @return String object
     */
    @Override
    public String toString() {
        return name;
    }

    /**
     * Return info about task in table
     * @return JTable object
     */
    @Override
    public JTable displayText() {
        String[] columnNames = {"Task information"};

        Object[][] data = {
                {"Task name: " + name},
                {"Duration (hr): " + duration},
                {"Mark: " + mark},
                {"Type task: " + type},
                {"Status of task: " + status.getFullNameStatus()},
        };

        JTable table = new JTable(data, columnNames);
        table.setBorder(BorderFactory.createEmptyBorder());
        return table;
    }
}
