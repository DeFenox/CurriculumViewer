package com.defenox.curview.model.handler;

import com.defenox.curview.model.entity.Task;
import org.xml.sax.*;
import org.xml.sax.helpers.DefaultHandler;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by defenox on 09.11.16.
 */
public class TaskHandler extends DefaultHandler {

    private static final String TAG_TASKS = "tasks";
    private static final String TAG_TASK_PROFILE = "profile_of_task";
    private static final String TAG_TASK_ID = "task_id";
    private static final String TAG_TASK_NAME = "name";
    private static final String TAG_TASK_DURATION = "duration";

    private boolean inTasks = false;
    private String valueOfElement;

    private Task task;

    private Map<String, Task> tasksList = new HashMap<>();

    /**
     * Get list of tasks
     * @return Map&lt;String, Course&gt; object
     */
    public Map<String, Task> getTasksList() {
        return tasksList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

        if(TaskHandler.TAG_TASKS.equalsIgnoreCase(qName)){
            inTasks = true;
        }

        if(TaskHandler.TAG_TASK_PROFILE.equalsIgnoreCase(qName) && inTasks){
            task = new Task();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if(TaskHandler.TAG_TASKS.equalsIgnoreCase(qName)){
            inTasks = false;
        }

        if(TaskHandler.TAG_TASK_ID.equalsIgnoreCase(qName) && inTasks){
            task.setId(valueOfElement);
        }

        if(TaskHandler.TAG_TASK_NAME.equalsIgnoreCase(qName) && inTasks){
            task.setName(valueOfElement);
        }

        if(TaskHandler.TAG_TASK_DURATION.equalsIgnoreCase(qName) && inTasks){
            task.setDuration(valueOfElement);
        }

        if(TaskHandler.TAG_TASK_PROFILE.equalsIgnoreCase(qName) && inTasks){
            tasksList.put(task.getId(), task);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        valueOfElement = new String(ch, start, length);
    }
}