package com.defenox.curview.model.handler;

import com.defenox.curview.model.entity.Student;
import org.xml.sax.*;
import org.xml.sax.helpers.DefaultHandler;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by defenox on 09.11.16.
 */
public class StudentHandler extends DefaultHandler {

    private static final String TAG_STUDENTS = "students";
    private static final String TAG_STUDENT_PROFILE = "profile_student";
    private static final String TAG_STUDENT_ID = "student_id";
    private static final String TAG_STUDENT_NAME = "full_name";
    private static final String TAG_STUDENT_CITY = "city";
    private static final String TAG_STUDENT_EMAIL = "email";
    private static final String TAG_STUDENT_START_DATE = "start_edu_date";
    private static final String TAG_STUDENT_SIGN = "isSignContract";

    private boolean inStudent = false;
    private String valueOfElement;

    private Student student;

    private Map<String, Student> studentsList = new HashMap<>();

    /**
     * Get list of student
     * @return Map&lt;String, Course&gt; object
     */
    public Map<String, Student> getStudentsList() {
        return studentsList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if(StudentHandler.TAG_STUDENTS.equalsIgnoreCase(qName)){
            inStudent = true;
        }

        if(StudentHandler.TAG_STUDENT_PROFILE.equalsIgnoreCase(qName) && inStudent){
            student = new Student();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if(StudentHandler.TAG_STUDENTS.equalsIgnoreCase(qName)){
            inStudent = false;
        }

        if(StudentHandler.TAG_STUDENT_ID.equalsIgnoreCase(qName) && inStudent){
            student.setId(valueOfElement);
        }

        if(StudentHandler.TAG_STUDENT_NAME.equalsIgnoreCase(qName) && inStudent){
            student.setFullName(valueOfElement);
        }

        if(StudentHandler.TAG_STUDENT_CITY.equalsIgnoreCase(qName) && inStudent){
            student.setCity(valueOfElement);
        }

        if(StudentHandler.TAG_STUDENT_EMAIL.equalsIgnoreCase(qName) && inStudent){
            student.setEmail(valueOfElement);
        }

        if(StudentHandler.TAG_STUDENT_START_DATE.equalsIgnoreCase(qName) && inStudent){
            try {
                student.setDate(valueOfElement);
            } catch (ParseException e) {
                throw new SAXException("Error: " + e.getMessage());
            }
        }

        if(StudentHandler.TAG_STUDENT_SIGN.equalsIgnoreCase(qName) && inStudent){
            student.setSignContract(Boolean.parseBoolean(valueOfElement));
        }

        if(StudentHandler.TAG_STUDENT_PROFILE.equalsIgnoreCase(qName) && inStudent){
            studentsList.put(student.getId(), student);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        valueOfElement = new String(ch, start, length);
    }
}