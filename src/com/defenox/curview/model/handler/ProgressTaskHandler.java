package com.defenox.curview.model.handler;

import com.defenox.curview.model.entity.PracticalStatus;
import com.defenox.curview.model.entity.Task;
import com.defenox.curview.model.entity.TheoryStatus;
import com.defenox.curview.model.entity.TypeTask;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by tokarev on 16.11.2016.
 */
public class ProgressTaskHandler extends DefaultHandler {

    private static final String TAG_TASK_RECORD_PROGRESS = "progress";
    private static final String TAG_TASK_RECORD = "record";
    private static final String TAG_TASK_RECORD_STUD_ID = "student_id";
    private static final String TAG_TASK_RECORD_TASK_ID = "task_id";
    private static final String TAG_TASK_RECORD_MARK = "mark";
    private static final String TAG_TASK_RECORD_THEORY = "TheoryTask";
    private static final String TAG_TASK_RECORD_PRACTICAL = "PracticalTask";
    private static final String TAG_TASK_RECORD_VALUE = "value";


    private boolean inProgress = false;

    private String valueOfElement;
    private String currentStudentID;

    private Task task;

    private Map<String, List<Task>> studentTaskList = new HashMap<>();

    /**
     * Get list of students and tasks
     * @return Map&lt;String, List&lt;Task&gt;&gt; object
     */
    public Map<String, List<Task>> getStudentTaskList() {
        return studentTaskList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (ProgressTaskHandler.TAG_TASK_RECORD_PROGRESS.equalsIgnoreCase(qName)) {
            inProgress = true;
        }

        if (ProgressTaskHandler.TAG_TASK_RECORD_THEORY.equalsIgnoreCase(qName) && inProgress) {
            task.setType(TypeTask.THEORY);
        }

        if (ProgressTaskHandler.TAG_TASK_RECORD_PRACTICAL.equalsIgnoreCase(qName) && inProgress) {
            task.setType(TypeTask.PRACTICAL);
        }

        if (ProgressTaskHandler.TAG_TASK_RECORD.equalsIgnoreCase(qName) && inProgress) {
            task = new Task();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (ProgressTaskHandler.TAG_TASK_RECORD_PROGRESS.equalsIgnoreCase(qName)) {
            inProgress = false;
        }

        if (ProgressTaskHandler.TAG_TASK_RECORD_STUD_ID.equalsIgnoreCase(qName) && inProgress) {
            currentStudentID = valueOfElement;
            if (!studentTaskList.containsKey(currentStudentID)) {
                studentTaskList.put(currentStudentID, new LinkedList<>());
            }
        }

        if (ProgressTaskHandler.TAG_TASK_RECORD_TASK_ID.equalsIgnoreCase(qName) && inProgress) {
            task.setId(valueOfElement);
        }

        if (ProgressTaskHandler.TAG_TASK_RECORD_MARK.equalsIgnoreCase(qName) && inProgress) {
            task.setMark(valueOfElement);
        }

        if (ProgressTaskHandler.TAG_TASK_RECORD_VALUE.equalsIgnoreCase(qName) && inProgress) {
            if (task.getType() == TypeTask.THEORY) {
                if (valueOfElement.equalsIgnoreCase(TheoryStatus.NOT_STARTED.getFullNameStatus())) {
                    task.setStatus(TheoryStatus.NOT_STARTED);
                } else if (valueOfElement.equalsIgnoreCase(TheoryStatus.IN_PROCESS.getFullNameStatus())) {
                    task.setStatus(TheoryStatus.IN_PROCESS);
                } else if (valueOfElement.equalsIgnoreCase(TheoryStatus.COMPLETE.getFullNameStatus())) {
                    task.setStatus(TheoryStatus.COMPLETE);
                }
            } else if (task.getType() == TypeTask.PRACTICAL) {
                if (valueOfElement.equalsIgnoreCase(PracticalStatus.NOT_STARTED.getFullNameStatus())) {
                    task.setStatus(PracticalStatus.NOT_STARTED);
                } else if (valueOfElement.equalsIgnoreCase(PracticalStatus.IN_PROCESS.getFullNameStatus())) {
                    task.setStatus(PracticalStatus.IN_PROCESS);
                } else if (valueOfElement.equalsIgnoreCase(PracticalStatus.REVIEWED.getFullNameStatus())) {
                    task.setStatus(PracticalStatus.REVIEWED);
                } else if (valueOfElement.equalsIgnoreCase(PracticalStatus.ACCEPTED.getFullNameStatus())) {
                    task.setStatus(PracticalStatus.ACCEPTED);
                }
            }
        }

        if (ProgressTaskHandler.TAG_TASK_RECORD.equalsIgnoreCase(qName) && inProgress) {
            studentTaskList.get(currentStudentID).add(task);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        valueOfElement = new String(ch, start, length);
    }
}
