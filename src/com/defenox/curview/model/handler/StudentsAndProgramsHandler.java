package com.defenox.curview.model.handler;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by DeFenox on 12.11.2016.
 */
public class StudentsAndProgramsHandler extends DefaultHandler {

    private static final String TAG_STUD_PROG = "students_and_programs";
    private static final String TAG_STUD_PROG_PROFILE = "training_program";
    private static final String TAG_STUD_PROG_ST_ID = "student_id";
    private static final String TAG_STUD_PROG_PR_ID = "program_id";
    
    private boolean inStudentProgram = false;

    private String valueOfElement;
    private String studentId;
    private String programId;

    private Map<String, String> studentProgramMap = new HashMap<>();

    /**
     * Get list of students program
     * @return Map&lt;String, String&gt; object
     */
    public Map<String, String> getStudentProgramMap() {
        return studentProgramMap;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if(StudentsAndProgramsHandler.TAG_STUD_PROG.equalsIgnoreCase(qName)){
            inStudentProgram = true;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if(StudentsAndProgramsHandler.TAG_STUD_PROG.equalsIgnoreCase(qName)){
            inStudentProgram = false;
        }

        if(StudentsAndProgramsHandler.TAG_STUD_PROG_ST_ID.equalsIgnoreCase(qName) && inStudentProgram){
            studentId = valueOfElement;
        }

        if(StudentsAndProgramsHandler.TAG_STUD_PROG_PR_ID.equalsIgnoreCase(qName) && inStudentProgram){
            programId = valueOfElement;
        }

        if(StudentsAndProgramsHandler.TAG_STUD_PROG_PROFILE.equalsIgnoreCase(qName) && inStudentProgram){
            studentProgramMap.put(studentId, programId);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        valueOfElement = new String(ch, start, length);
    }
}
