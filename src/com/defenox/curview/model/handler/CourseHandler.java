package com.defenox.curview.model.handler;

import com.defenox.curview.model.entity.Course;
import org.xml.sax.*;
import org.xml.sax.helpers.DefaultHandler;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by defenox on 09.11.16.
 */
public class CourseHandler extends DefaultHandler {

    private static final String TAG_COURSE = "courses";
    private static final String TAG_COURSE_PROFILE = "profile_of_course";
    private static final String TAG_COURSE_ID = "courses_id";
    private static final String TAG_COURSE_NAME = "name";
    private static final String TAG_COURSE_AUTHOR = "author";
    private static final String TAG_COURSE_DATE = "date_of_creation";
    private static final String TAG_COURSE_LIST_TASKS = "list_of_tasks";

    private boolean inCourse = false;
    private String valueOfElement;

    private Course course;

    private String[] taskList;

    /**
     * Get list of tasks
     * @return String[] object
     */
    public String[] getTaskList() {
        return taskList;
    }

    private Map<String, Course> CourseList = new HashMap<>();

    /**
     * Get list of courses
     * @return Map&lt;String, Course&gt; object
     */
    public Map<String, Course> getCourseList() {
        return CourseList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

        if(CourseHandler.TAG_COURSE.equalsIgnoreCase(qName)){
            inCourse = true;
        }

        if(CourseHandler.TAG_COURSE_PROFILE.equalsIgnoreCase(qName) && inCourse){
            course = new Course();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {

        if(CourseHandler.TAG_COURSE.equalsIgnoreCase(qName)){
            inCourse = false;
        }

        if(CourseHandler.TAG_COURSE_ID.equalsIgnoreCase(qName) && inCourse){
            course.setId(valueOfElement);
        }

        if(CourseHandler.TAG_COURSE_NAME.equalsIgnoreCase(qName) && inCourse){
            course.setName(valueOfElement);
        }

        if(CourseHandler.TAG_COURSE_AUTHOR.equalsIgnoreCase(qName) && inCourse){
            course.setAuthor(valueOfElement);
        }

        if(CourseHandler.TAG_COURSE_DATE.equalsIgnoreCase(qName) && inCourse){
            try {
                course.setDateOfCreation(valueOfElement);
            } catch (ParseException e) {
                throw new SAXException("Error: " + e.getMessage());
            }
        }

        if(CourseHandler.TAG_COURSE_LIST_TASKS.equalsIgnoreCase(qName) && inCourse){
            taskList = valueOfElement.split("\\s+");
            course.setIdCourses(taskList);
        }

        if(CourseHandler.TAG_COURSE_PROFILE.equalsIgnoreCase(qName) && inCourse){
            CourseList.put(course.getId(), course);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        valueOfElement = new String(ch, start, length);
    }
}
