package com.defenox.curview.model.handler;

import com.defenox.curview.model.entity.Program;
import org.xml.sax.*;
import org.xml.sax.helpers.DefaultHandler;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by defenox on 09.11.16.
 */
public class ProgramHandler extends DefaultHandler {

    private static final String TAG_PROGRAMS= "programs";
    private static final String TAG_PROGRAM_PROFILE = "profile_of_program";
    private static final String TAG_PROGRAM_ID = "program_id";
    private static final String TAG_PROGRAM_NAME = "name";
    private static final String TAG_PROGRAM_AUTHOR = "author";
    private static final String TAG_PROGRAM_DATE = "date_of_creation";
    private static final String TAG_PROGRAM_LIST_OF_COURSES = "list_of_courses";

    private boolean inProgram = false;
    private String valueOfElement;

    private Program program;

    private Map<String, Program> programList = new HashMap<>();
    private String[] courseList;

    /**
     * Get list of program
     * @return Map&lt;String, Program&gt; object
     */
    public Map<String, Program> getProgramList() {
        return programList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

        if(ProgramHandler.TAG_PROGRAMS.equalsIgnoreCase(qName)){
            inProgram = true;
        }

        if(ProgramHandler.TAG_PROGRAM_PROFILE.equalsIgnoreCase(qName) && inProgram){
            program = new Program();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if(ProgramHandler.TAG_PROGRAMS.equalsIgnoreCase(qName)){
            inProgram = false;
        }

        if(ProgramHandler.TAG_PROGRAM_ID.equalsIgnoreCase(qName) && inProgram){
            program.setId(valueOfElement);
        }

        if(ProgramHandler.TAG_PROGRAM_NAME.equalsIgnoreCase(qName) && inProgram){
            program.setName(valueOfElement);
        }

        if(ProgramHandler.TAG_PROGRAM_AUTHOR.equalsIgnoreCase(qName) && inProgram){
            program.setAuthor(valueOfElement);
        }

        if(ProgramHandler.TAG_PROGRAM_DATE.equalsIgnoreCase(qName) && inProgram){
            try {
                program.setDateOfCreation(valueOfElement);
            } catch (ParseException e) {
                throw new SAXException("Error: " + e.getMessage());
            }
        }

        if(ProgramHandler.TAG_PROGRAM_LIST_OF_COURSES.equalsIgnoreCase(qName) && inProgram){
            courseList = valueOfElement.split("\\s+");
            program.setIdCourses(courseList);
        }

        if(ProgramHandler.TAG_PROGRAM_PROFILE.equalsIgnoreCase(qName) && inProgram){
            programList.put(program.getId(), program);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        valueOfElement = new String(ch, start, length);
    }
}
