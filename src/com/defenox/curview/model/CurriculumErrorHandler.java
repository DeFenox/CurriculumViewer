package com.defenox.curview.model;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import java.io.PrintStream;

/**
 * Created by defenox on 02.11.16.
 */
public class CurriculumErrorHandler implements ErrorHandler {

    private PrintStream out;

    CurriculumErrorHandler(PrintStream out) {
        this.out = out;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void warning(SAXParseException e) throws SAXException {
        out.println("Warning: " + getParseExceptionInfo(e));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void error(SAXParseException e) throws SAXException {
        String message = "Error: " + getParseExceptionInfo(e);
        throw new SAXException(message);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void fatalError(SAXParseException e) throws SAXException {
        String message = "Fatal Error: " + getParseExceptionInfo(e);
        throw new SAXException(message);
    }

    /**
     * Return exception info
     * @param e SAXParseException object
     * @return String object
     */
    private String getParseExceptionInfo(SAXParseException e) {
        String systemId = e.getSystemId();
        if (systemId == null) {
            systemId = "null";
        }
        return "URI=" + systemId + " Line=" + e.getLineNumber()
                + ": " + e.getMessage();
    }
}