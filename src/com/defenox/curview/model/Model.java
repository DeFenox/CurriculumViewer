package com.defenox.curview.model;

import com.defenox.curview.model.entity.Course;
import com.defenox.curview.model.entity.Program;
import com.defenox.curview.model.entity.Student;
import com.defenox.curview.model.entity.Task;
import com.defenox.curview.model.handler.*;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 * Created by defenox on 27.10.16.
 */
public class Model {

    private File file;
    private SAXParserFactory parserFactory;

    private SAXParser parser;

    private StudentHandler studentHandler = new StudentHandler();
    private ProgramHandler programHandler = new ProgramHandler();
    private CourseHandler courseHandler = new CourseHandler();
    private TaskHandler taskHandler = new TaskHandler();
    private StudentsAndProgramsHandler studentsAndProgramsHandler = new StudentsAndProgramsHandler();
    private ProgressTaskHandler progressTaskHandler = new ProgressTaskHandler();

    static final String JAXP_SCHEMA_LANGUAGE = "http://java.sun.com/xml/jaxp/properties/schemaLanguage";
    static final String JAXP_SCHEMA_SOURCE = "http://java.sun.com/xml/jaxp/properties/schemaSource";
    private InputStream schemaSource = this.getClass().getResourceAsStream("/res/Report.xsd");

    private JTree tree = new JTree();

    public Model() {

    }

    /**
     * Set xml file of data
     * @param file File object
     */
    public void setFile(File file) {
        this.file = file;
    }

    /**
     * Start parse XML file
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public void parseXML() throws ParserConfigurationException, SAXException, IOException {
        parserFactory = SAXParserFactory.newInstance();
        parserFactory.setNamespaceAware(true);
        parserFactory.setValidating(true);


        parser = parserFactory.newSAXParser();

        parser.setProperty(JAXP_SCHEMA_LANGUAGE, XMLConstants.W3C_XML_SCHEMA_NS_URI);

        if (schemaSource != null) {
            parser.setProperty(JAXP_SCHEMA_SOURCE, schemaSource);
        }

        XMLReader reader = parser.getXMLReader();
        reader.setErrorHandler(new CurriculumErrorHandler(System.err));

        reader.setContentHandler(studentHandler);
        reader.parse(file.getAbsolutePath());

        reader.setContentHandler(programHandler);
        reader.parse(file.getAbsolutePath());

        reader.setContentHandler(courseHandler);
        reader.parse(file.getAbsolutePath());

        reader.setContentHandler(taskHandler);
        reader.parse(file.getAbsolutePath());

        reader.setContentHandler(studentsAndProgramsHandler);
        reader.parse(file.getAbsolutePath());

        reader.setContentHandler(progressTaskHandler);
        reader.parse(file.getAbsolutePath());

        createTree();
    }

    private void createTree() {
        DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode("Students");
        DefaultMutableTreeNode studentsNode;
        DefaultMutableTreeNode programNode;
        DefaultMutableTreeNode courseNode;
        DefaultMutableTreeNode taskNode;

        Map<String, Student> studentsList = studentHandler.getStudentsList();
        Map<String, Program> programList = programHandler.getProgramList();
        Map<String, Course> courseList = courseHandler.getCourseList();
        Map<String, Task> taskList = taskHandler.getTasksList();
        Map<String, List<Task>> studentsTaskList = progressTaskHandler.getStudentTaskList();

        //create student's nodes

        Map<String, String> studentProgramMap = studentsAndProgramsHandler.getStudentProgramMap();
        for (Map.Entry<String, Student> studentEntry : studentsList.entrySet()) {

            studentsNode = new DefaultMutableTreeNode(studentEntry.getValue());
            List<Task> tasksStudent = studentsTaskList.get(studentEntry.getValue().getId());

            if (studentProgramMap.containsKey(studentEntry.getKey())) {

                Program programTemp = programList.get(studentProgramMap.get(studentEntry.getKey()));
                programNode = new DefaultMutableTreeNode(programTemp);
                Map<String, Course> courses = programTemp.getListCourse();

                for (Map.Entry<String, Course> courseEntry : courses.entrySet()) {
                    Course courseTemp = courseList.get(courseEntry.getKey());
                    courses.put(courseEntry.getKey(), courseTemp);
                    courseNode = new DefaultMutableTreeNode(courseTemp);
                    Map<String, Task> tasks = courseTemp.getListTask();

                    for (Map.Entry<String, Task> taskEntry : tasks.entrySet()) {
                        Task taskTemp = taskList.get(taskEntry.getKey());

                        for (Task taskElement : tasksStudent) {
                            if (taskTemp.getId().equalsIgnoreCase(taskElement.getId())) {
                                taskTemp.setMark(taskElement.getMark());
                                taskTemp.setType(taskElement.getType());
                                taskTemp.setStatus(taskElement.getStatus());
                            }
                        }

                        courseTemp.getListTask().put(taskEntry.getKey(), taskTemp);
                        taskNode = new DefaultMutableTreeNode(taskTemp);
                        courseNode.add(taskNode);
                    }
                    programNode.add(courseNode);
                }
                studentEntry.getValue().getProgramList().add(programTemp);
                studentsNode.add(programNode);
            }
           rootNode.add(studentsNode);
        }
        tree = new JTree(rootNode);
    }

    /**
     * Return tree of model
     * @return JTree object
     */
    public JTree getTree() {
        return tree;
    }
}